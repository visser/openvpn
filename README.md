This repository contains the OpenVPN client configuration for the OpenVPN server
at the Faculty of Science.

Instructions: https://wiki.cncz.science.ru.nl/Vpn#OpenVPN_.5Bvoor.5D.5Bfor.5D_Linux_.26_MacOS